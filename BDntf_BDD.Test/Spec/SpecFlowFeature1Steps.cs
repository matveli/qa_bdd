﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;

namespace BDntf_BDD.Test.Spec
{
    [Binding]
    public class SpecFlowFeature1Steps
    {
        BDNotifier bn = new BDNotifier();

        [Given(@"I have my notifier")]
        public void GivenIHaveMyNotifier()
        {
            Assert.IsNotNull(bn);
        }

        [Given(@"I have my notifier friends book")]
        public void GivenIHaveMyNotifierFriendsBook()
        {
            Assert.IsNotNull(bn.friendsBook);
        }


        [When(@"I creating friend")]
        public void WhenICreatingFriend()
        {
            bn.ExecuteAdd();
        }
        
        [Then(@"Friends count is (.*)")]
        public void ThenFriendsCountIs(int value)
        {
            int actual = bn.FriendsCount();

            Assert.AreEqual(value, actual);
        }

        [When(@"I creating friend with (.*) and (.*)")]
        public void WhenICreatingFriendWithAnd(string name, string bd)
        {
            bn.AddFriend(name, bd);
        }

        [Then(@"I checking bd of friend (.*) is (.*)")]
        public void WhenICheckingBdOfFriend(string name, string result)
        {
            var actual = bn.BDOfFriend(name);

            Assert.AreEqual(DateTime.Parse(result), actual);
        }
        

    }
}
