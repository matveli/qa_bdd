﻿Feature: SpecFlowFeature1
	

Scenario: add new friend
    Given I have my notifier
	And I have my notifier friends book
    When I creating friend 
    Then Friends count is 1

Scenario Outline: add friends group
	Given I have my notifier
	And I have my notifier friends book
    When I creating friend with <name> and <bd>
    Then Friends count is 1
	Examples: 
	| name  | bd         |
	| Ellie | 12.09.1999 |
	| Eva   | 08.22.2001 |
	| Adam  | 03.01.2000 |

Scenario Outline: bd of friend
    Given I have my notifier
	And I have my notifier friends book
    When I creating friend with <name> and <bd>
	Then I checking bd of friend <name> is <bd>
	Examples: 
	| name  | bd         |
	| Ellie | 12.09.1999 |
	| Eva   | 08.22.2001 |
	| Adam  | 03.01.2000 |