﻿using System;
using System.Collections.Generic;

namespace BDntf_BDD
{
    public class Friend
    {
        public string name;
        public DateTime Birthday;

        public Friend() { }

        public Friend(string _name, DateTime _bd)
        {
            name = _name;
            Birthday = _bd;
        }
    }

    public class BDNotifier
    {
        public List<Friend> friendsBook;

        public BDNotifier()
        {
            friendsBook = new List<Friend>();
        }

        public void ExecuteAdd()
        {
            friendsBook.Add(new Friend("Eva", new DateTime(1994, 12, 04)));
        }

        public int FriendsCount()
        {
            return friendsBook.Count;
        }

        public void AddFriend(string name, string bd)
        {
            friendsBook.Add(new Friend(name, DateTime.Parse(bd)));
        }

        public DateTime BDOfFriend(string name)
        {
            foreach (var friend in friendsBook)
            {
                if (name == friend.name)
                {
                    return friend.Birthday;
                }
            }
            return new DateTime();
        }
    }
}
